<?php
/**************************************************************************************************/
/*                                                                                                */
/* Clase LayerDBAdmin                                                                             */
/*     Clase para la comunicación con la base de datos                                            */
/*                                                                                                */
/* Listado de funciones:                                                                          */
/*     actualiza($nTabla, $vCampos, $vWhere, $limit = '', $debug = false)                         */
/* 	   conexion() 																				  */
/*     elimina($nTabla, $vWhere, $logico = 'and', $debug = false)                                 */
/*     escapes($cadena)                                                                           */
/*     getLastId()                                                                                */
/*     inserta($nTabla, $vCampos, $duplicate = '', $debug = false)                                */
/*     query($sql)                                                                                */
/*     selecciona($nTabla, $vCampos, $vWhere, $logico = 'AND', $orderBy = '', $limit = '',        */
/*                $groupBy = '', $having = '', $debug = false)                                    */
/*     transaccionBEGIN()                                                                         */
/*     transaccionCOMMIT()                                                                        */
/*     transaccionROLLBACK()                                                                      */
/*                                                                                                */
/**************************************************************************************************/

	// Variables de conexión al servidor de bbdd
	$servidorDb = 'localhost';

	// Base de datos general
	$nombreDb = 'avaibook';
	$usuarioDb = 'root';
	$claveDb = '';

class LayerDBAdmin{

////////////////////////////////////////////////////////////////////////////////////////////////////

	// Actualiza registros de la tabla indicada
	// @param string $nTabla: nombre de la tabla a actualizar
	// @param array $vCampos: vector con nombre de campo y valor de los campos que queremos actualizar (nuevos valores):
	// 				$vCampos['nombreCampo'] => ['valorCampo']
	// @param array $vWhere: vector con nombre de campo y valor de los campos que determinen el registro (clave primaria):
	// 				$vWhere['nombreCampo'] => ['valorCampo']
	// @param bool $limit: sentencia limit.
	// @param bool $debug: si estamos en modo debug.
	// @return devuelve el número de filas modificadas
	// @throws Exception, Exception

	static function actualiza($nTabla, $vCampos, $vWhere, $limit = '', $debug = false){
		global $connDb;

		self::conexion();

		if ((!empty($nTabla)) and (!empty($vCampos)) and (is_array($vCampos)) and (!empty($vWhere)) and (is_array($vWhere))){

			$sql_ini = 'UPDATE '.$nTabla;
			$sql_set = '';
			$sql_where = '';

			// Compongo la clausula set
			foreach ($vCampos as $key => $val){

				$val = html_entity_decode($val, ENT_QUOTES, 'UTF-8');

				if (!empty($sql_set))
					$sql_set .= ', ';

				// Si el valor ya viene con comillas en los extremos, se quitan para hacer el escapado y luego se vuelven a poner
				if ((substr($val, 0, 1) == '"') and (substr($val, -1) == '"'))
					$val = '"'.self::escapes(substr($val, 1, strlen($val)-2)).'"';
				elseif ((substr($val, 0, 1) == "'") and (substr($val, -1) == "'"))
					$val = "'".self::escapes(substr($val, 1, strlen($val)-2))."'";
				else
					$val = self::escapes($val);

				$sql_set .= $key.' = '.$val;

			}

			// Compongo la clausula where
			if (is_array($vWhere)){
				foreach ($vWhere as $key => $val){
					if (!empty($sql_where))
						$sql_where .= ' and ';
					$sql_where.= $key.' = '.$val;
				}
			}

			if (!empty($sql_set)) $sql_set = ' SET '.$sql_set;
			if (!empty($sql_where)) $sql_where = ' WHERE '.$sql_where;
			if (!empty($limit)) $sql_limit = ' LIMIT '.$limit;

			// Monto la consulta definitiva
			$sql = $sql_ini.$sql_set.$sql_where.$sql_limit;

			if ($debug)
				echo $sql;

			if (self::query($sql))
				return $connDb->affected_rows;
			else
				throw new Exception('Error MySQLi '.$connDb->errno . ' : ' . $connDb->error);

		}else
			throw new Exception('Actualiza: Parámetros incorrectos');

	}// actualiza

////////////////////////////////////////////////////////////////////////////////////////////////////

	// Comprueba la conexión con la BBDD y si no existe la abre de nuevo

	static function conexion(){

		global $connDb;
		global $__srv_actual;
		global $servidorDb;
		global $nombreDb;
		global $usuarioDb;
		global $claveDb;

		// Prueba desconexión base de datos por límite de tiempo
		if ((!$connDb) || (!$connDb->ping())){

			// Conexión y selección de la base de datos
			$connDb = @new mysqli($servidorDb, $usuarioDb, $claveDb, $nombreDb);

			if ($connDb->connect_error){
				self::log_error_db('['.$__srv_actual.'] '.$connDb->errno.': '.$connDb->error);
				exit();
			}else
				$connDb->set_charset('utf8');

		}

	} // conexion

////////////////////////////////////////////////////////////////////////////////////////////////////


	// Elimina registros de la tabla indicada
	// @param string $nTabla: nombre de la tabla de donde se eliminarán los registros
	// @param array $vWhere: vector con nombre de campo y valor de los campos que determinen el registro (clave primaria):
	// 				$vWhere['nombreCampo'] => ['valorCampo']
	// @return El número de filas que se han eliminado
	// @throws Exception, Exception

	static function elimina($nTabla, $vWhere, $logico = 'and', $debug = false){
		global $connDb;

		self::conexion();

		if ((!empty($nTabla)) and (!empty($vWhere)) and (is_array($vWhere))){

			$sql_ini = 'DELETE FROM '.$nTabla;
			$sql_where = '';

			// Compongo la clausula where
			foreach ($vWhere as $key => $val){

				if (!empty($sql_where))
					$sql_where .= ' '.$logico.' ';

				// Si el valor ya viene con comillas en los extremos, se quitan para hacer el escapado y luego se vuelven a poner
				if ((substr($val, 0, 1) == '"') and (substr($val, -1) == '"'))
					$val = '"'.self::escapes(substr($val, 1, strlen($val)-2)).'"';
				elseif ((substr($val, 0, 1) == "'") and (substr($val, -1) == "'"))
					$val = "'".self::escapes(substr($val, 1, strlen($val)-2))."'";
				else
					$val = self::escapes($val);

				$sql_where.= $key." = ".$val;

			}

			if (!empty($sql_where)) $sql_where = ' WHERE '.$sql_where;

			// Monto la consulta definitiva
			$sql = $sql_ini.$sql_where;

			if ($debug)
				echo $sql;

			if (self::query($sql))
				return $connDb->affected_rows;
			else
				throw new Exception('Error MySQLi '.$connDb->errno . ' : ' . $connDb->error);

		}else
			throw new Exception('Elimina: Parámetros incorrectos');

	}// elimina

////////////////////////////////////////////////////////////////////////////////////////////////////

	// Escapa la cadena para su uso en una consulta sql
	// @param string $cadena: Cadena de datos a escapar

	static function escapes($cadena){

		global $connDb;
		global $path_raiz;

		self::conexion();

		// Si la cadena contiene uno de los valores reservados, redirigimos a errorInesperado
		if ((stripos($cadena, ' information_schema') === false) and
			(stripos($cadena, '0x20information_schema') === false) and
			(stripos($cadena, '%20information_schema') === false) and
			(stripos($cadena, 'schemata') === false) and
			(stripos($cadena, 'select ') === false) and
			(stripos($cadena, 'select0x20') === false) and
			(stripos($cadena, 'select%20') === false)
		){

			return $connDb->real_escape_string($cadena);

		}else{
			echo '<strong>Error filtrado sql: '.$_SERVER['PHP_SELF'].' '.$_SERVER['QUERY_STRING'].'</strong>';
			exit();
		}

	}// escapes

////////////////////////////////////////////////////////////////////////////////////////////////////

	// Devuelve el último id insertado (si es autonumérico)
	static function getLastId(){
		global $connDb;
		// self::conexion();
		return $connDb->insert_id;
	}// getLastId

////////////////////////////////////////////////////////////////////////////////////////////////////


	// Inserta los registros indicados en la tabla indicada
	// @param string $nTabla: nombre de la tabla donde se insertarán los registros
	// @param array $vCampos: vector con nombre de campo y valor de los campos que van a ser insertados:
	// 				$vCampos['nombreCampo'] => ['valorCampo']
	// @param bool $debug: si estamos en modo debug.
	// @return El número de filas que se han insertado
	// @throws Exception, Exception

	static function inserta($nTabla, $vCampos, $duplicate = '', $debug = false){
		global $connDb;

		self::conexion();

		if ((!empty($nTabla)) and (is_array($vCampos))){

			$sql_ini = 'INSERT INTO '.$nTabla;
			$sql_names = '';
			$sql_values = '';
			$prime = true;

			// Compongo la clausula de inserción
			foreach ($vCampos as $key => $val){

				$val = html_entity_decode($val, ENT_QUOTES, 'UTF-8');

				if (!empty($sql_names))
					$sql_names .= ', ';

				// if (!empty($sql_values))
				if (!$prime)
					$sql_values .= ', ';

				// Si el valor ya viene con comillas en los extremos, se quitan para hacer el escapado y luego se vuelven a poner
				if ((substr($val, 0, 1) == '"') and (substr($val, -1) == '"'))
					$val = '"'.self::escapes(substr($val, 1, strlen($val)-2)).'"';
				elseif ((substr($val, 0, 1) == "'") and (substr($val, -1) == "'"))
					$val = "'".self::escapes(substr($val, 1, strlen($val)-2))."'";
				else
					$val = self::escapes($val);

				$sql_names .= $key;
				$sql_values .= $val;

				$prime = false;

			}

			// Monto la consulta definitiva
			$sql = $sql_ini.' ('.$sql_names.') VALUES ('.$sql_values.')';

			if ($duplicate)
				$sql .= ' ON DUPLICATE KEY UPDATE '.$duplicate;

			if ($debug)
				echo $sql;

			if (self::query($sql))
				return $connDb->affected_rows;
			else
				throw new Exception('Error MySQLi '.$connDb->errno . ' : ' . $connDb->error);

		}else
			throw new Exception('Inserta: Parámetros incorrectos');

	}// inserta

////////////////////////////////////////////////////////////////////////////////////////////////////

	// Esta función escribe el fichero de errores de conexión a la BBDD
	static function log_error_db($log_txt){

		if (PRODUCCION){

			$fichero_log = '/var/www/vhosts/avaibook.com/log/'.date('Y-m-d').'_log_error_db.txt';

			$fp = @fopen($fichero_log, 'a');
			if ($fp){
				fwrite($fp, '['.date('d-m-Y H:i:s').'] '.$log_txt. ' == DETALLE: '.$_SERVER['HTTP_X_FORWARDED_FOR'].';'.$_SERVER['REQUEST_URI'].';'.$_SERVER['HTTP_REFERER'].';'.PHP_EOL);
				fclose($fp);
			}

		}

	}// log_error_db

////////////////////////////////////////////////////////////////////////////////////////////////////

	// Función principal. Todas acaban llamando a esta para realizar la consulta
	// Realiza una prueba de conexión antes de ejecutar la consulta para evitar un error 2006 de mysql
	// @param string $sql: Sentencia sql a ejecutar

	static function query($sql){
		global $connDb;
		self::conexion();
		return $connDb->query($sql);

	}// query

////////////////////////////////////////////////////////////////////////////////////////////////////

	// Selecciona registros de la tabla indicada
	// @param string $nTabla: nombre de la tabla donde buscar
	// @param array $vCampos: nombres de los campos a seleccionar
	// @param array[array] $vWhere: en cada posicion hay un array asociativo con 3 componenes que determinan las clausulas where:
	// 			["campo"]: nombre del campo a incluir en la calusula where, ej. "email"
	//			["signo"]: signo de la comparación de la clausula, ej "like", o "="
	//			["valor"]: valor a comparar, ej. "'%esa.com'"
	// @param string $logico: operador logico a utilizar entre las clausulas where (AND, OR)
	// @param string $orderBy: valor de la clausula de ordenacion a utilizar (ej: "id asc")
	// @param string $limit: valor de la clausula limit a utilizar (ej: "0, 6")
	// @param string $groupBy: la sentencia group by si la hubiera.
	// @param string $having: la sentencia having si la hubiera.
	// @param bool $debug: si estamos en modo debug.
	// @return devuelve un vector en el que cada registro obtenido está en una componente
	// @throws Exception, Exception

	static function selecciona($nTabla, $vCampos, $vWhere, $logico = 'AND', $orderBy = '', $limit = '', $groupBy = '', $having = '', $debug = false){
		global $connDb;
		self::conexion();

		if ((!empty($nTabla)) and (!empty($vCampos)) and (is_array($vCampos))){

			$sql_ini = 'SELECT ';
			$sql_sel = '';
			$sql_where = '';

			// Compongo los campos a obtener
			foreach ($vCampos as $key => $val){
				if (!empty($sql_sel))
					$sql_sel .= ', ';

				$sql_sel .= $val;
			}

			// Compongo la clausula where
			foreach ($vWhere as $key => $val){
				
				if (!empty($sql_where))
					$sql_where .= ' '.$logico.' ';

				// Si el valor ya viene con comillas en los extremos, se quitan para hacer el escapado y luego se vuelven a poner
				if ((substr($val['valor'], 0, 1) == '"') and (substr($val['valor'], -1) == '"'))
					$val['valor'] = '"'.self::escapes(substr($val['valor'], 1, strlen($val['valor'])-2)).'"';
				elseif ((substr($val['valor'], 0, 1) == "'") and (substr($val['valor'], -1) == "'"))
					$val['valor'] = "'".self::escapes(substr($val['valor'], 1, strlen($val['valor'])-2))."'";
				else
					$val['valor'] = self::escapes($val['valor']);

				$sql_where .= $val['campo'].' '.$val['signo'].' '.$val['valor'];

			}

			if (!empty($sql_where)) $sql_where = ' WHERE '.$sql_where;
			// if (!empty($groupBy)) $sql_group = ' GROUP BY '.$groupBy ;
			// if (!empty($having)) $sql_having = ' HAVING '.$having;
			// if (!empty($orderBy)) $sql_order = ' ORDER BY '.$orderBy;
			// if (!empty($limit)) $sql_limit = ' LIMIT '.$limit;
			!empty($groupBy) ? $sql_group = ' GROUP BY '.$groupBy : $sql_group = '' ;
			!empty($having) ? $sql_having = ' HAVING '.$having : $sql_having = '' ;
			!empty($orderBy) ? $sql_order = ' ORDER BY '.$orderBy : $sql_order = '' ;
			!empty($limit) ? $sql_limit = ' LIMIT '.$limit : $sql_limit = '' ;

			// Monto la consulta definitiva
			$sql = $sql_ini.$sql_sel.' FROM '.$nTabla.$sql_where.' '.$sql_group.$sql_having.$sql_order.$sql_limit;

			if ($debug)
				echo $sql;

			if ($rsSel = self::query($sql)){

				// Pongo los datos de la consulta en un vector
				while ($rwSel = $rsSel->fetch_assoc())
					$listaResultados[] = $rwSel;

				if (count($listaResultados) > 0){
					foreach ($listaResultados as $resultado){
						unset($aux);
						foreach ($resultado as $key => $result)
							$aux[$key] = htmlspecialchars($result, ENT_QUOTES, 'UTF-8');	// Sustituimos unos caracteres problemáticos por su versión HTML
						$listaDefinitiva[] = $aux;
					}
				}else
					$listaDefinitiva = $listaResultados;

				return $listaDefinitiva;

			}else
				throw new Exception('Error MySQLi '.$connDb->errno . ' : ' . $connDb->error);

		}else
			throw new Exception('Selecciona: Parámetros incorrectos');

	}// selecciona

////////////////////////////////////////////////////////////////////////////////////////////////////

	// Comienza una transacción
	static function transaccionBEGIN(){
		self::query('BEGIN');
		// global $connDb;
		// $connDb->begin_transaction(); // >= PHP 5.5 (Actualmente 5.4)
	}// transaccionBEGIN

////////////////////////////////////////////////////////////////////////////////////////////////////

	// Confirma todas las consultas desde el último begin
	static function transaccionCOMMIT(){
		self::query('COMMIT');
		// global $connDb;
		// $connDb->commit(); // >= PHP 5.5 (Actualmente 5.4)
	}// transaccionCOMMIT

////////////////////////////////////////////////////////////////////////////////////////////////////

	// Deshace todas las consultas desde el último begin
	static function transaccionROLLBACK(){
		self::query('ROLLBACK');
		// global $connDb;
		// $connDb->rollback(); // >= PHP 5.5 (Actualmente 5.4)
	}// transaccionROLLBACK

////////////////////////////////////////////////////////////////////////////////////////////////////

}// LayerDBAdmin (Clase)

?>