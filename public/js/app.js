function createDatatableWithoutExport(id) {
    var table = $('#' + id).DataTable({
        dom: "<'row'<'col-sm-6'l><'col-sm-6'f>><'row'<'col-sm-12'>><'row'tr>" + "<'row'<'col-sm-4'i><'col-sm-8'p>>",
    });
    return table;
}