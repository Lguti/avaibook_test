<meta charset="UTF-8" />

<meta name="author" content="AvaiBook.com" />
<meta name="keywords" content="calendario, disponibilidad, calendario de disponibilidad, reservas on-line, sistema centralizado, booking, pago on-line, herramientas, propietarios, alojamientos turisticos, gratis" />
<meta name="description" content="Avaibook.com: Sistema Centralizado de Disponibilidad y Reservas On-line para el sector turístico de alquiler vacacional. Es un sistema gratuito que permite a los propietarios de alojamientos turísticos disfrutar de sistema de calendario de disponibilidad ,y sistema de reservas on-line, para su propia web y para los portales en los que se anuncia." />
<meta name="robots" content="all" />
<meta name="language" content="es_ES" />
<meta name="distribution" content="global" />
<meta name="audience" content="all" />
<meta name="viewport" content="">
<meta name="author-mail" content="webmaster@avaibook.com" />
<meta name="publisher" content="AvaiBook.com" />
<meta name="image_src" content="images" />

<title>AvaiTest - Powered by AvaiBook</title>

<link rel="icon" type="image/png" href="https://www.avaibook.com/images/favicon.png">


<link href="<?php echo $path_raiz; ?>css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo $path_raiz; ?>font-awesome/css/font-awesome.min.css"/>
<link rel="stylesheet" href="<?php echo $path_raiz; ?>../public/assets/datatables/css/dataTables.bootstrap.min.css">