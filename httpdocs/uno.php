<?php

	$path_raiz = './';

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

?>

<!DOCTYPE html>
<html>

	<head>
		<?php include_once($path_raiz.'includes/head.php');?>
	</head>

	<body>

		<div class="content-main clearfix">

			<h1>Configuración</h1>

			<section>

                <article class="introduccion">
                    <p><strong>Aquí te mostramos todas las reglas de cobro que tienes programadas</strong>, haciendo distinción <strong>por alojamiento y origen</strong> de la reserva (motor de reservas AvaiBook y Booking.com o Expedia).</p>
                </article>


               <article class="listado">
               		<div class="resultado clearfix">
	               		<div class="nombre" id="nombre_4084" style="background-color: #afdcd7; cursor: pointer"><strong>4084.- Apto Completo Playa</strong></div>
	                    <div class="linea clearfix">
	                        <div>Para reservas recibidas por el motor de AvaiBook, TripAdvisor [Instant Booking]</div>
	                        <div>1 % de anticipo<span class="rojo"> + 0 reglas configuradas</span></div>
	                        <div id="lanza_ajax" cod_propietario="2"><a href="#">Lanza ajax</a></div>
	                        <div id="datos_propi"></div>
	                    </div>
	                </div>
	                <hr>
	                <div class="resultado clearfix">
	                	<div class="nombre" id="nombre_4085" style="background-color: #afdcd7; cursor: pointer"><strong>4085.- Casa Parcial Ciudad</strong></div>
	                    <div class="linea clearfix">
	                        <div>Para reservas recibidas por el motor de AvaiBook, TripAdvisor [Instant Booking]</div>
	                        <div>1 % de anticipo<span class="rojo"> + 0 reglas configuradas</span></div>
	                        <div><a href="#">Editar</a></div>
	                    </div>
	                </div>
	                <hr>
	                <div class="resultado clearfix">
	                	<div class="nombre" id="nombre_34198" style="background-color: #afdcd7; cursor: pointer"><strong>34198.- Completo - On request</strong></div>
	                    <div class="linea clearfix">
	                        <div>Para reservas recibidas por el motor de AvaiBook</div>
	                        <div>50 % de anticipo<span class="rojo"> + 0 reglas configuradas</span></div>
	                        <div><a href="#">Editar</a></div>
	                    </div>
	                </div>
	                <hr>
	                <div class="resultado clearfix">
	                	<div class="nombre" id="nombre_4245" style="background-color: #afdcd7; cursor: pointer"><strong>4245.- Completo Casa Ciudads</strong></div>
	                    <div class="linea clearfix">
	                        <div>Para reservas recibidas por el motor de AvaiBook</div>
	                        <div>25 % de anticipo<span class="rojo"> + 0 reglas configuradas</span></div>
	                        <div><a href="#">Editar</a></div>
	                    </div>
	                </div>
	                <hr>
	                <div class="resultado clearfix">
	                	<div class="nombre" id="nombre_52189" style="background-color: #afdcd7; cursor: pointer"><strong>52189.- dsfsdf</strong></div>
	                    <div class="linea clearfix">
	                        <div>Para reservas recibidas por el motor de AvaiBook</div>
	                        <div>50 % de anticipo<span class="rojo"> + 0 reglas configuradas</span></div>
	                        <div><a href="#">Editar</a></div>
	                    </div>
	                </div>
	                <br><br>
	                <button>Cambiar color</button>
                </article>

			</section>

		</div>

	<?php include_once($path_raiz.'includes/footer.php');?>
	</body>
	<script>

		$(document).ready(() => {
			$('.nombre').on('click', (event) => {
				// OBTENGO ID DEL EVENTO CLICK
				const { id } = event.target
				const alerta = id.split('_')
				alert(alerta[1])
			})

		})

		$('#lanza_ajax').on('click', (event) => {
			const cod_propietario = $('#lanza_ajax').attr('cod_propietario')
			$.ajax({
				url: "./ajax/ajax_uno.php",
				data: {'id': cod_propietario},
				type: "POST",
				success: (response) => {
				const { email, nombre, f_alta, estado } = JSON.parse(response)[0]
				// LE DOY A LA FECHA EL FORMATO DD/MM/YYY
				const date_format = new Date(f_alta)
				const day = date_format.getDate()
				const month = date_format.getMonth()
				const year = date_format.getFullYear()
				const fecha_propietario = `${day}/${month}/${year}`

				// RENDERIZO DATOS EN EL DOM
				$('#datos_propi').html(`
					<h4>datos del propietario</h4>
					<p> <strong> Nombre : ${nombre} </strong> </p>
					<p> <strong> Email : ${email} </strong> </p>
					<p> <strong> Fecha de alta : ${fecha_propietario} </strong> </p>
					<p> <strong> Estado : ${estado} </strong> </p>
				`)
				},
				error: (error, textStatus) => {
					alert(textStatus)
				}
			})
		})
			
	</script>
</html>