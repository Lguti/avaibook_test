<?php
	$path_raiz = './';

	require_once $path_raiz.'../inc/layerDbAdmin.inc.php';

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// OBETENGO INFORMACIÓN DEL VALOR DE LOS INPUTS
	if(count($_POST) > 0) {
		$nTabla = 'propietarios';
		$vCampos = [
			'1' => 'nombre',
			'2' => 'email',
			'3' => 'f_alta',
			'4' => 'estado',
			'5' => 'comision',
		];
		$vWhere [] = [
			"campo" => "email",
			"signo" => "LIKE",
			"valor" => "'%".$_POST['email']."%'"
		] ;

		if(isset($_POST['estado']) && $_POST['estado'] == 1){
			$_POST['estado'] = 'ACTIVO';
			$vWhere [] = [
				"campo" => "AND estado",
				"signo" => "=",
				"valor" => "'".$_POST['estado']."'"
			];
		} else if (isset($_POST['estado']) && $_POST['estado'] == 0){
			$_POST['estado'] = 'BAJA';
			$vWhere [] = [
				"campo" => "AND estado",
				"signo" => "=",
				"valor" => "'".$_POST['estado']."'"
			];
		}

		$vResultadosConsulta = LayerDBAdmin::selecciona($nTabla, $vCampos, $vWhere, $orderBy='', $groupBy='',$having='');
		$listaResultadosFiltrados = $vResultadosConsulta;

	} 
	
	// SI NO HAY INFORMACIÓN EN LOS INPUT MUESTRO TODO
	$sql = 'SELECT * FROM propietarios';
	$vResultadosConsulta = LayerDBAdmin::query($sql);
	while ($rwSel = $vResultadosConsulta->fetch_assoc())
	$listaResultados[] = $rwSel;
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
?>

<!DOCTYPE html>
<html>

	<head>
		<?php include_once($path_raiz.'includes/head.php');?>
	</head>

	<body>

		<div class="content-main clearfix">

			<h1>Listado de propietarios</h1>
			<div class="row" style="margin-bottom: 10px">
				<form action="#" method="post">
					<div class="col-lg-3">
						<label for="email">Filtrar por email</label>
						<input class="form-control" type="text" name="email" placeholder="Email">
					</div>
					<div class="col-lg-3">
						<label for="estado">Seleccione estado</label>
						<select class="form-control" name="estado" id="">
							<option selected disabled>Seleccione un estado</option>
							<option value="1">ACTIVO</option>
							<option value="0">BAJA</option>
						</select>
					</div>
					<button class="btn btn-success" type="submit" style="margin-top: 2%">Aplicar</button>
				</form>
			</div>
			<div class="table-responsive">
				<table id="propietarios_table" class="table table-bordered table-hover" data-name="cool-table">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Email</th>
							<th>Fecha de Alta</th>
							<th>Estado</th>
							<th>Comisión</th>
						</tr>
					</thead>
					<tbody>
						<?php if(isset($listaResultadosFiltrados)): ?>
						<?php foreach($listaResultadosFiltrados as $resultado): ?>
						<tr>
							<td><?= $resultado['nombre'] ?></td>
							<td> <a href="mailto:<?= $resultado['email'] ?>"><?= $resultado['email'] ?></a></td>
							<td><?= date('d/m/Y.', strtotime($resultado['f_alta'])) ?></td>
							<td><?= $resultado['estado'] ?></td>
							<td><?= $resultado['comision'] ?></td>
						</tr>
						<?php endforeach; ?>
					<?php else: ?>
					<?php foreach($listaResultados as $resultado): ?>
					<tr>
						<td><?= $resultado['nombre'] ?></td>
						<td> <a href="mailto:<?= $resultado['email'] ?>"><?= $resultado['email'] ?></a></td>
						<td><?= date('d/m/Y.', strtotime($resultado['f_alta'])) ?></td>
						<td><?= $resultado['estado'] ?></td>
						<td><?= $resultado['comision'] ?></td>
					</tr>
					<?php endforeach; ?>
				<?php endif; ?>
				</tbody>
			</table>
		</div>
	</div>
		

	</body>
	<?php include_once($path_raiz.'includes/footer.php');?>
	<script>
		createDatatableWithoutExport('propietarios_table')
	</script>
</html>

