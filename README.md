# avaibook_test
## Avaibook consultas SQL :wrench:

- [x] **Media de los anticipos cobrados de los alojamientos activos**
```
SELECT 
    AVG(alojamientos.anticipo) AS media
FROM
    avaibook.alojamientos
WHERE
    alojamientos.estado = 'ACTIVO';

```

- [x] **Número total de habitaciones de cada hotel (sólo activos), mostrando Nombre de hotel – total de habitaciones**
```
SELECT 
    alojamientos.nombre AS hotel,
    COUNT(habitaciones.cod_alojamiento) AS total_habitaciones
FROM
    avaibook.habitaciones
        LEFT JOIN
    avaibook.alojamientos ON habitaciones.cod_alojamiento = alojamientos.id
WHERE
    alojamientos.estado = 'ACTIVO'
GROUP BY hotel;

```

- [x] **Propietarios registrados en septiembre**
```
SELECT 
    email AS email_propietario,
    propietarios.nombre AS nomnbre_propietario,
    monthname(f_alta) AS fecha_registro
FROM
    avaibook.propietarios
WHERE
    f_alta LIKE '%-09-%';

```

- [x] **Número total de propietarios registrados cada mes**
```
SELECT 
    DATE_FORMAT(propietarios.f_alta, '%Y-%m') AS mes,
    propietarios.id AS total_registros
FROM
    avaibook.propietarios
GROUP BY mes
ORDER BY mes ASC

```

- [x] **Propietarios (Email y total de alojamientos) que tienen 2 o más alojamientos activos**
```
SELECT 
    email, COUNT(alojamientos.cod_propietario) AS total_alojs
FROM
    avaibook.propietarios
        LEFT JOIN
    avaibook.alojamientos ON propietarios.id = alojamientos.cod_propietario
WHERE
    alojamientos.estado = 'ACTIVO'
GROUP BY email
HAVING COUNT(alojamientos.id) >= 2;

```

## Avaibook PHP :rocket:

- [x] **Recupera todos los propietarios y pinta un listado en una tabla con esos datos.**

- [x] **Crea un filtro encima de la tabla que permita buscar por email (tiene que buscar por parte del email, no por coincidencia exacta) y otro por estado (desplegable).**

## Avaibook Jquery :boom:

- [x] **Haz que al hacer clic sobre cualquier div que el id que comienza por “nombre_” se muestre un alert con el resto del id (si el id es “nombre_4085” que el alert muestre 4085).**

- [x] Al hacer clic sobre el div con id **“lanza_ajax”**, realizar una llamada Ajax al fichero “ajax_uno.php” (incluido en la carpeta Ajax del directorio raiz). **El Ajax debe recibir el parámetro “cod_propietario”**, el cual contendrá el valor del atributo con el mismo nombre en el div que se clica (“lanza_ajax”).
Usa ese código para recuperar el propietario con el mismo ID en la base de datos que te hemos facilitado al principio y **rellenar con los datos obtenidos el div con id “datos_propi”** en el fichero inicial (uno.php).
